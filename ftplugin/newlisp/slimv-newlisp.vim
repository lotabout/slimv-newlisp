" slimv-scheme.vim:
"               newlisp filetype plugin for Slimv
" Version:      0.9.6
" Last Change:  25 Mar 2012
" Maintainer:   Tamas Kovacs <kovisoft at gmail dot com>
" License:      This file is placed in the public domain.
"               No warranty, express or implied.
"               *** ***   Use At-Your-Own-Risk!   *** ***
"
" =====================================================================
"
"  Load Once:
if exists("b:did_ftplugin") || exists("g:slimv_disable_newlisp")
    finish
endif

" ---------- Begin part loaded once ----------
if !exists( 'g:slimv_newlisp_loaded' )

let g:slimv_newlisp_loaded = 1

" Try to autodetect newlisp executable
" Returns list [Scheme executable, Scheme implementation]
function! b:SlimvAutodetect( preferred )
    " only newlisp
    if executable( 'newlisp' )
        " newlisp
        return ['newlisp', 'newlisp']
    endif

    return ['', '']
endfunction

" Try to find out the Scheme implementation
function! b:SlimvImplementation()
    if exists( 'g:slimv_impl' ) && g:slimv_impl != ''
        " Return Lisp implementation if defined
        return tolower( g:slimv_impl )
    endif

    return 'newlisp'
endfunction

" Try to autodetect SWANK and build the command to load the SWANK server
function! b:SlimvSwankLoader()
    if g:slimv_impl == 'newlisp'
        let swanks = split( globpath( &runtimepath, 'swank-newlisp/swank-newlisp.lsp'), '\n' )
        if len( swanks ) == 0
            return ''
        endif
        return g:slimv_lisp . ' -n ' . swanks[0] . ' -e "(swank:create-server)"'
    endif
    return ''
endfunction

" Filetype specific initialization for the REPL buffer
function! b:SlimvInitRepl()
    set filetype=newlisp
endfunction

" Lookup symbol in the Hyperspec
function! b:SlimvHyperspecLookup( word, exact, all )
    " No Hyperspec support for Scheme at the moment
    let symbol = []
    return symbol
endfunction

" Source Slimv general part
runtime ftplugin/**/slimv.vim

endif "!exists( 'g:slimv_scheme_loaded' )
" ---------- End of part loaded once ----------

runtime ftplugin/**/lisp.vim

" The balloonexpr of MIT-Scheme is broken. Disable it.
"let g:slimv_balloon = 0

" The fuzzy completion of MIT-Scheme is broken. Disable it.
"let g:slimv_simple_compl = 1

" Must be called for each lisp buffer
call SlimvInitBuffer()

" Don't load another plugin for this buffer
let b:did_ftplugin = 1

